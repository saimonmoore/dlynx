import { all } from "redux-saga/effects";
import { watchLinks } from "./dat";

export default function* rootSaga() {
  yield all([watchLinks()]);
}
