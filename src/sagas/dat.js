import { channel } from "redux-saga";

import {
  call,
  fork,
  put,
  take,
  takeEvery,
  takeLatest
} from "redux-saga/effects";

import {
  localDatArchive,
  addLink,
  removeLink,
  readArchive,
  updateLink,
  updateLinkKeyword
} from "../dat";

import {
  ADD_LINK,
  ADD_LINK_SUCCESS,
  ADD_LINK_FAILURE,
  REMOVE_LINK,
  REMOVE_LINK_SUCCESS,
  REMOVE_LINK_FAILURE,
  BOOT_LINKS,
  BOOT_LINKS_SUCCESS,
  BOOT_LINKS_FAILURE,
  UPDATE_LINK,
  UPDATE_LINK_SUCCESS,
  UPDATE_LINK_FAILURE,
  PARSE_LINK,
  PARSE_LINK_SUCCESS,
  PARSE_LINK_FAILURE,
  UPDATE_KEYWORD,
  UPDATE_KEYWORD_SUCCESS,
  UPDATE_KEYWORD_FAILURE
} from "../redux/actionTypes";

import { addBootLink } from "../redux/actions";
import parseLink from "../linkParser";

let datArchive;

const getLocalDatArchive = async () => {
  if (!datArchive) datArchive = await localDatArchive();

  return datArchive;
};

export function* addLinkToDatArchive(action) {
  try {
    const datArchive = yield call(getLocalDatArchive);

    yield call(addLink, datArchive, action.payload);
    yield put({ type: ADD_LINK_SUCCESS });
    yield put({ type: PARSE_LINK, url: action.payload.url });
  } catch (error) {
    yield put({ type: ADD_LINK_FAILURE, error });
  }
}

export function* updateLinkInDatArchive(action) {
  try {
    const datArchive = yield call(getLocalDatArchive);

    yield call(updateLink, datArchive, action.payload);
    yield put({ type: UPDATE_LINK_SUCCESS });
  } catch (error) {
    yield put({ type: UPDATE_LINK_FAILURE, error });
  }
}

export function* updateKeywordInLink(action) {
  try {
    const datArchive = yield call(getLocalDatArchive);

    yield call(updateLinkKeyword, datArchive, action.payload);
    yield put({ type: UPDATE_KEYWORD_SUCCESS });
  } catch (error) {
    yield put({ type: UPDATE_KEYWORD_FAILURE, error });
  }
}
export function* parseLinkHandler({ url }) {
  try {
    const parsedData = yield call(parseLink, url);
    console.log("[parseLinkHandler] got: ", parsedData);
    yield put({ type: UPDATE_LINK, payload: { url, ...parsedData } });
    yield put({ type: PARSE_LINK_SUCCESS, url });
  } catch (error) {
    yield put({ type: PARSE_LINK_FAILURE, error });
  }
}

export function* removeLinkFromDatArchive(action) {
  try {
    const datArchive = yield call(getLocalDatArchive);

    const data = yield call(removeLink, datArchive, action.payload);
    yield put({ type: REMOVE_LINK_SUCCESS, data });
  } catch (error) {
    yield put({ type: REMOVE_LINK_FAILURE, error });
  }
}

function* consumeBootLinks(bootLinksChannel) {
  while (true) {
    const link = yield take(bootLinksChannel);
    // process the request
    const res = yield put(addBootLink(link));
    console.log(res);
  }
}

function* bootStoreFromDatArchive() {
  const CONCURRENT_WORKERS = 2;
  const bootLinksChannel = yield call(channel);
  let workers = [];

  function* scheduleWorkers() {
    workers = [];
    for (let i = 0; i < CONCURRENT_WORKERS; i++) {
      const worker = yield fork(consumeBootLinks, bootLinksChannel);
      workers.push(worker);
    }
  }

  yield* scheduleWorkers();

  function* putToChannel(chan, task) {
    return yield put(chan, task);
  }

  function* generateBootLinkAdds() {
    try {
      const datArchive = yield call(getLocalDatArchive);
      const archive = yield call(readArchive, datArchive);
      const localLinks = archive["@excsm"].links;

      const urls = Object.keys(localLinks);
      for (const url of urls) {
        yield fork(putToChannel, bootLinksChannel, localLinks[url]);
      }

      yield put({ type: BOOT_LINKS_SUCCESS });
    } catch (error) {
      console.log("error booting...", error);
      yield put({ type: BOOT_LINKS_FAILURE, error });
    }
  }

  yield fork(generateBootLinkAdds);
}

export function* watchLinks() {
  yield takeLatest(BOOT_LINKS, bootStoreFromDatArchive);
  yield takeEvery(PARSE_LINK, parseLinkHandler);
  yield takeEvery(UPDATE_LINK, updateLinkInDatArchive);
  yield takeEvery(ADD_LINK, addLinkToDatArchive);
  yield takeEvery(REMOVE_LINK, removeLinkFromDatArchive);
  yield takeEvery(UPDATE_KEYWORD, updateKeywordInLink);
}
