import React from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";

import Link from "./Link";

const StyledList = styled.ul`
  list-style-type: square;
  margin-left: 0px;
  padding-left: 0px;
`;

const LinkList = () => {
  const links = useSelector(state => state.links.allLinks);
  const filteredLinks = useSelector(state => state.links.filteredLinks);

  let linksForList;

  linksForList = filteredLinks && filteredLinks.length && filteredLinks;
  linksForList = linksForList || links;

  return (
    <StyledList>
      {linksForList && linksForList.length
        ? linksForList.map((link, index) => {
            return <Link key={`todo-${index}`} link={link} />;
          })
        : "Add a link.."}
    </StyledList>
  );
};

export default LinkList;
