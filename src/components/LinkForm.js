import React, { useState } from "react";
import { useDispatch } from "react-redux";
import styled from "styled-components";

import { addLink } from "../redux/actions";

const LinkInputLabel = styled.label`
  width: 60%;
  margin-left: 0px;
  padding-left: 0px;
`;

const LinkInput = styled.input`
  background-color: aqua;
  height: 2em;
  width: 100%;
`;

const onEnter = (fn, cleanUpFn) => event => {
  if (event.key === "Enter") {
    fn();
    cleanUpFn();
  }
};

const LinkForm = () => {
  const [url, setUrl] = useState("");
  const dispatch = useDispatch();

  return (
    <LinkInputLabel>
      <LinkInput
        type="text"
        onChange={event => setUrl(event.target.value)}
        onKeyUp={onEnter(() => dispatch(addLink(url)), () => setUrl(""))}
        placeholder="Add url.."
        value={url}
      />
    </LinkInputLabel>
  );
};

export default LinkForm;
