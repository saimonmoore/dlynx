import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";

import {
  removeLink,
  updateKeyword,
  filterLinks,
  clearFilters
} from "../redux/actions";

import { KEYWORD_FILTER } from "../redux/reducers/filters";

const LinkItem = styled.li`
  font-size: 2em;
  text-align: left;
  display: flex;
  margin-left: 0px;
  padding-left: 0px;
`;

const LeftLinkArea = styled.div``;
const RightLinkArea = styled.div``;

const LinkA = styled.a`
  color: aqua;
`;

const RemoveLinkButton = styled.a`
  color: aqua;
  cursor: pointer;
  font-size: medium;
  margin-left: 50px;
`;

const KeywordPillElement = styled.li`
  background-color: #3cb7ac;
  color: black;
  font-size: small;
  font-variant-caps: all-small-caps;
  font-kerning: auto;
  font-style: italic;
  font-weight: bolder;
  font-stretch: expanded;
  width: fit-content;
  padding: 0px 5px 0px 5px;
  border-radius: 10px;
  margin-top: 2px;
  margin-right: 5px;
  padding-top: 0px;
  text-align: center;
  text-shadow: 1px 3px 5px black;
  cursor: pointer;

  &:hover {
    font-size: medium;
  }
`;

const KeywordPillListElement = styled.ul`
  list-style-type: none;
  display: flex;
  margin-top: 10px;
  text-align: left;
`;

const SelectedKeywordPillListElement = styled.span`
  background-color: orange;
`;

const KeywordPillListInput = styled.input``;

const onEnter = (fn, cleanUpFn) => event => {
  if (event.key === "Enter") {
    fn();
    cleanUpFn && cleanUpFn();
  }
};

const KeywordPill = ({ edit_mode, link, keyword }) => {
  const [editingKeyword, setToggleEditingKeyword] = useState(false);
  const [newKeyword, setNewKeyword] = useState(keyword);
  const [selected, setSelected] = useState(false);
  const dispatch = useDispatch();

  const toggleEditingKeyword = () => {
    setToggleEditingKeyword(!editingKeyword);

    console.log("===> edit mode & toggle editing keyword");
  };

  const dispatchFilterLinks = () => {
    setSelected(true);
    dispatch(filterLinks({ filter: KEYWORD_FILTER, term: keyword }));
    console.log("===> !selected & dispatch filter links");
  };

  const dispatchClearFilters = () => {
    setSelected(false);
    dispatch(clearFilters());
    console.log("===> selected & dispatch clear links");
  };

  return (
    <KeywordPillElement
      onClick={() =>
        (edit_mode && !editingKeyword && toggleEditingKeyword()) ||
        (!edit_mode && !selected && dispatchFilterLinks()) ||
        (!edit_mode && selected && dispatchClearFilters())
      }
    >
      {editingKeyword && (
        <KeywordPillListInput
          type="text"
          autoFocus={true}
          onChange={event => setNewKeyword(event.target.value)}
          onKeyUp={onEnter(
            () => dispatch(updateKeyword(link, keyword, newKeyword)),
            () => setToggleEditingKeyword(false)
          )}
          value={newKeyword}
        />
      )}
      {!editingKeyword && !selected && <span>{keyword}</span>}
      {!editingKeyword && selected && (
        <SelectedKeywordPillListElement>
          {keyword}
        </SelectedKeywordPillListElement>
      )}
    </KeywordPillElement>
  );
};

const KeywordPillList = ({ edit_mode, link, keywords }) => {
  return (
    <KeywordPillListElement>
      {keywords.map((keyword, index) => (
        <KeywordPill
          edit_mode={edit_mode}
          link={link}
          keyword={keyword}
          key={index}
        />
      ))}
    </KeywordPillListElement>
  );
};

const Link = ({ link }) => {
  const [hovering, setHovering] = useState(false);
  const dispatch = useDispatch();
  const settings = useSelector(state => state.settings);

  return (
    <LinkItem
      onMouseOver={() => setHovering(true)}
      onMouseLeave={() => setHovering(false)}
    >
      <LeftLinkArea>
        <LinkA href={link.url} target="blank">
          {link.url}
        </LinkA>
        {link.keywords && link.keywords.length && (
          <KeywordPillList
            edit_mode={settings.editing}
            link={link}
            keywords={link.keywords.filter(k => k && k.length)}
          />
        )}
      </LeftLinkArea>
      <RightLinkArea>
        {settings.editing && (
          <RemoveLinkButton onClick={() => dispatch(removeLink(link.url))}>
            <span role="img" aria-label="remove">
              ❌
            </span>
          </RemoveLinkButton>
        )}
      </RightLinkArea>
    </LinkItem>
  );
};

export default Link;
