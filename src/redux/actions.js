import {
  ADD_LINK,
  REMOVE_LINK,
  BOOT_LINKS,
  ADD_BOOT_LINK,
  TOGGLE_EDITING,
  UPDATE_KEYWORD,
  FILTER_LINKS,
  CLEAR_FILTERS
} from "./actionTypes";

export const addLink = (url, metadata, keywords) => ({
  type: ADD_LINK,
  payload: { url, metadata, keywords }
});

export const removeLink = url => ({
  type: REMOVE_LINK,
  payload: { url }
});

export const bootLinks = () => ({
  type: BOOT_LINKS
});

export const addBootLink = ({ url, metadata, keywords }) => ({
  type: ADD_BOOT_LINK,
  payload: { url, metadata, keywords }
});

export const updateKeyword = (link, keyword, newKeyword) => ({
  type: UPDATE_KEYWORD,
  payload: { link, keyword, newKeyword }
});

// Settings

export const toggleEditing = () => ({
  type: TOGGLE_EDITING
});

// Filters

export const filterLinks = ({ filter, term }) => ({
  type: FILTER_LINKS,
  payload: { filter, term }
});

export const clearFilters = () => ({
  type: CLEAR_FILTERS
});
