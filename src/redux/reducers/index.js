import { combineReducers } from "redux";
import reduceReducers from "reduce-reducers";
import settings from "./settings";
import links from "./links";
import filters from "./filters";

const initialState = {
  allLinks: [],
  byUrls: {}
};

export default combineReducers({
  settings,
  links: reduceReducers(initialState, links, filters)
});
