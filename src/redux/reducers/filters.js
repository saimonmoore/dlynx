import { FILTER_LINKS, CLEAR_FILTERS } from "../actionTypes";

export const KEYWORD_FILTER = "keyword";

const initialState = {
  filteredLinks: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CLEAR_FILTERS: {
      return { ...state, filteredLinks: [] };
    }

    case FILTER_LINKS: {
      const { filter, term } = action.payload;
      const { allLinks } = state;
      let filteredLinks;

      switch (filter) {
        case KEYWORD_FILTER: {
          filteredLinks = allLinks.filter(link =>
            link.keywords.some(keyword => keyword.startsWith(term))
          );

          break;
        }

        default:
          filteredLinks = state;
      }

      return {
        ...state,
        filteredLinks
      };
    }

    default:
      return state;
  }
}
