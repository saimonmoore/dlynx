import { TOGGLE_EDITING } from "../actionTypes";

const initialState = {
  editing: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case TOGGLE_EDITING: {
      return {
        ...state,
        editing: !state.editing
      };
    }

    default:
      return state;
  }
}
