import { ADD_LINK, REMOVE_LINK, ADD_BOOT_LINK } from "../actionTypes";

const removeUrl = (links, url) => {
  delete links[url];
  return links;
};

export default function(state, action) {
  switch (action.type) {
    case ADD_LINK: {
      const { url } = action.payload;

      return {
        ...state,
        allLinks: [...state.allLinks, { ...action.payload }],
        byUrls: {
          ...state.byUrls,
          [url]: { ...action.payload }
        }
      };
    }

    case ADD_BOOT_LINK: {
      const { url } = action.payload;

      return {
        ...state,
        allLinks: [...state.allLinks, { ...action.payload }],
        byUrls: {
          ...state.byUrls,
          [url]: { ...action.payload }
        }
      };
    }

    case REMOVE_LINK: {
      const { url } = action.payload;
      return {
        ...state,
        allLinks: state.allLinks.filter(entry => entry.url !== url),
        byUrls: removeUrl(state.allLinks, url)
      };
    }
    default:
      return state;
  }
}
