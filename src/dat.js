// import DatArchive from "dat-archive-web";

const DatArchive = window.DatArchive;

const linksArchivePath = "/data/links.json";

const prepareDatArchive = async datArchive => {
  await datArchive.mkdir("/data");
  await datArchive.writeFile(
    linksArchivePath,
    JSON.stringify({
      "@excsm": { links: {}, friends: {} }
    })
  );
};

const initDatRepository = async () => {
  const datArchive = await DatArchive.create({
    title: "DLYNX",
    description: "A link sharing app...",
    type: ["dlynx-archive"],
    author: "@excsm"
  });
  console.log("===> Created new persisted datArchive: ", datArchive);
  await prepareDatArchive(datArchive);

  localStorage.setItem("dlynx", datArchive.url);

  return datArchive;
};

export const readArchive = async datArchive => {
  const data = await datArchive.readFile(linksArchivePath);

  return JSON.parse(data);
};

export const addLink = async (datArchive, payload) => {
  const archive = await readArchive(datArchive);
  const localLinks = archive["@excsm"].links;
  localLinks[payload.url] = payload;

  await datArchive.writeFile(linksArchivePath, JSON.stringify(archive));

  console.log("[dat-addLink] wrote:", archive);

  return true;
};

export const updateLink = async (datArchive, payload) => {
  const archive = await readArchive(datArchive);
  const localLinks = archive["@excsm"].links;
  const link = localLinks[payload.url];

  if (!link) localLinks[payload.url] = payload;
  if (link) localLinks[payload.url] = { ...link, ...payload };

  await datArchive.writeFile(linksArchivePath, JSON.stringify(archive));

  console.log("[dat-updateLink] wrote:", archive);

  return true;
};

export const removeLink = async (datArchive, { url }) => {
  const archive = await readArchive(datArchive);
  const localLinks = archive["@excsm"].links;
  delete localLinks[url];

  await datArchive.writeFile(linksArchivePath, JSON.stringify(archive));

  console.log("[dat-removeLink] wrote:", archive);

  return true;
};

export const updateLinkKeyword = async (datArchive, payload) => {
  const archive = await readArchive(datArchive);
  const localLinks = archive["@excsm"].links;
  const { link } = payload;
  const datLink = localLinks[link.url];

  const { keywords } = datLink;
  var foundIndex = keywords.findIndex(keyword => keyword === payload.keyword);
  keywords[foundIndex] = payload.newKeyword;
  datLink.keywords = keywords.filter(keyword => keyword && keyword.length);

  await datArchive.writeFile(linksArchivePath, JSON.stringify(archive));

  console.log("[dat-updateLinkKeyword] wrote:", archive);

  return true;
};

const datLog = async datArchive => {
  console.log(
    "===> dlynx archive data after preparation:",
    await datArchive.readFile(linksArchivePath)
  );
  console.log("===> dlynx archive info:", await datArchive.getInfo());
  // console.log("===> dlynx archive history:", await datArchive.history());
};

export const loadDatArchive = async datArchiveUrl => {
  const archive = await DatArchive.load(datArchiveUrl);
  console.log("===> Loaded datArchive: ", archive);
  return archive;
};

export const localDatArchive = async () => {
  let datArchive;

  const datArchiveUrl = localStorage.getItem("dlynx");

  if (!datArchiveUrl) {
    console.log("No datArchive found! Creating new one: ");
    datArchive = initDatRepository();
  } else {
    console.log("Found existing datArchive for: ", datArchiveUrl);
  }

  datArchive = datArchive || (await loadDatArchive(datArchiveUrl));
  console.log("datArchive url: ", datArchiveUrl, " key: ", datArchive.key);

  datLog(datArchive);

  console.log(
    "[dlynx-archive-event] SETTING EVENT LISTENERS to:",
    datArchive,
    datArchive._events
  );
  datArchive.addEventListener("network-changed", payload =>
    console.log("[dlynx-archive-event] network-changed: ", { payload })
  );

  datArchive.addEventListener("download", payload =>
    console.log("[dlynx-archive-event] download: ", { payload })
  );

  datArchive.addEventListener("upload", payload =>
    console.log("[dlynx-archive-event] upload: ", { payload })
  );

  datArchive.addEventListener("sync", payload =>
    console.log("[dlynx-archive-event] sync: ", { payload })
  );

  console.log(
    "[dlynx-archive-event] DONE SETTING EVENT LISTENERS to:",
    datArchive,
    datArchive._events
  );

  const linksEmitter = datArchive.watch(linksArchivePath);
  linksEmitter.addEventListener("invalidated", ({ path }) =>
    console.log("[dlynx-archive-watch-links-event] invalidated", { path })
  );

  linksEmitter.addEventListener("changed", ({ path }) =>
    console.log("[dlynx-archive-watch-links-event] changed", { path })
  );

  // todo close linksEmitter

  return datArchive;
};
