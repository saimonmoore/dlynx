import React from "react";
import { Provider, useDispatch } from "react-redux";

import styled from "styled-components";

import store from "./redux/store";
// import DatContextProvider from "./contexts/dat";
import { toggleEditing } from "./redux/actions";
import LinkForm from "./components/LinkForm";
import LinksList from "./components/LinksList";

const Title = styled.h1`
  font-size: 1.5em;
  color: palevioletred;
`;

const AppFrame = styled.div`
  background-color: #282c34;
  text-align: center;
`;

const AppBodyLeft = styled.div`
  width: 150px;
`;
const AppBodyCenter = styled.div``;
const AppBodyRight = styled.div`
  width: 50px;
`;

const AppHeaderLeft = styled.div`
  width: 150px;
`;
const AppHeaderCenter = styled.div``;
const AppHeaderRight = styled.div`
  width: 50px;
  display: flex;
  justify-content: right;
  padding-top: 15px;
`;

const AppBody = styled.div`
  background-color: #282c34;
  display: flex;
  flex-direction: row;
  align-items: left;
  justify-content: center;
  font-size: calc(10px + 2vmin);
  color: white;
  height: 100%;
`;

const AppHeader = styled.header`
  background-color: #282c34;
  display: flex;
  color: white;
  justify-content: space-between;
`;

const EditLink = styled.a`
  cursor: pointer;
  &:hover {
    color: lawngreen;
  }
`;

const Header = () => {
  const dispatch = useDispatch();
  return (
    <AppHeader>
      <AppHeaderLeft />
      <AppHeaderCenter>
        <Title>DLYNX</Title>
      </AppHeaderCenter>
      <AppHeaderRight>
        <EditLink onClick={() => dispatch(toggleEditing())}>Edit</EditLink>
      </AppHeaderRight>
    </AppHeader>
  );
};

const App = () => {
  return (
    <Provider store={store}>
      <AppFrame>
        <Header />
        <AppBody>
          <AppBodyLeft />
          <AppBodyCenter>
            <LinkForm />
            <LinksList />
          </AppBodyCenter>
          <AppBodyRight />
        </AppBody>
      </AppFrame>
    </Provider>
  );
};

export default App;
