import $ from "jquery";
import retext from "retext";
import retext_keywords from "retext-keywords";
import retext_english from "retext-english";
import nlcstToString from "nlcst-to-string";

const metadataRuleSets = {
  description: {
    rules: [
      [
        'meta[property="og:description"]',
        element => element.getAttribute("content")
      ],
      [
        'meta[property="schema:description"]',
        element => element.getAttribute("content")
      ],
      [
        'meta[property="twitter:description"]',
        element => element.getAttribute("content")
      ],
      [
        'meta[name="twitter:description"]',
        element => element.getAttribute("content")
      ],
      ['meta[name="description"]', element => element.getAttribute("content")]
    ]
  },
  keywords: {
    rules: [
      ['meta[name="keywords" i]', element => element.getAttribute("content")]
    ],
    processors: [
      (keywords, context) => keywords.split(",").map(keyword => keyword.trim())
    ]
  },
  title: {
    rules: [
      ['meta[property="og:title"]', element => element.getAttribute("content")],
      [
        'meta[name="twitter:title"]',
        element => element.getAttribute("content")
      ],
      [
        'meta[property="twitter:title"]',
        element => element.getAttribute("content")
      ],
      ['meta[name="hdl"]', element => element.getAttribute("content")],
      ["title", element => element.text]
    ]
  },
  language: {
    rules: [
      ["html[lang]", element => element.getAttribute("lang")],
      ['meta[name="language" i]', element => element.getAttribute("content")]
    ],
    processors: [(language, context) => language.split("-")[0]]
  },
  type: {
    rules: [
      ['meta[property="og:type"]', element => element.getAttribute("content")]
    ]
  }
};

const buildRuleSet = ruleSet => {
  return (doc, context) => {
    let maxScore = 0;
    let maxValue;

    for (let currRule = 0; currRule < ruleSet.rules.length; currRule++) {
      const [query, handler] = ruleSet.rules[currRule];

      const elements = doc.filter(query).toArray();

      if (elements.length) {
        for (const element of elements) {
          let score = ruleSet.rules.length - currRule;

          if (score > maxScore) {
            maxScore = score;
            maxValue = handler(element);
          }
        }
      }
    }

    if (!maxValue && ruleSet.defaultValue) {
      maxValue = ruleSet.defaultValue(context);
    }

    if (maxValue) {
      if (ruleSet.processors) {
        for (const processor of ruleSet.processors) {
          maxValue = processor(maxValue, context);
        }
      }

      if (maxValue.trim) {
        maxValue = maxValue.trim();
      }

      return maxValue;
    }
  };
};

const getMetadata = (html, url) => {
  const doc = $(html);
  const metadata = {};
  const context = {
    url
  };

  const ruleSets = metadataRuleSets;

  Object.keys(ruleSets).forEach(ruleSetKey => {
    const ruleSet = ruleSets[ruleSetKey];
    const builtRuleSet = buildRuleSet(ruleSet);

    try {
      metadata[ruleSetKey] = builtRuleSet(doc, context);
    } catch (error) {
      console.log("[linkParser] error trying to parse ruleSetKey:", ruleSetKey);
    }
  });

  return metadata;
};

const stringifyNlcst = nlcst => nlcstToString(nlcst);
const flatten = array => [].concat(...array);
const compact = array => array.filter(element => element !== null);
const filterMetadataForKeywords = metadata =>
  Object.keys(metadata).filter(filteredKey => !["type"].includes(filteredKey));

const parseKeywords = async metadata => {
  const tags = new Set();
  const keywordParser = retext()
    .use(retext_english)
    .use(retext_keywords);

  const terms = await Promise.all(
    filterMetadataForKeywords(metadata).map(key => {
      const string = metadata[key];
      return new Promise((resolve, reject) => {
        keywordParser.process(string, (error, file) => {
          if (error) {
            console.log("[parseKeywords] failed to parse: ", string, error);
            reject(null);
          }

          const keywords = file.data.keywords.map(keyword =>
            stringifyNlcst(keyword.matches[0].node)
          );

          const keyphrases = file.data.keyphrases.map(phrase =>
            phrase.matches[0].nodes.map(stringifyNlcst).join("-")
          );

          resolve([...keywords, ...keyphrases]);
        });
      });
    })
  );

  compact(flatten(terms)).forEach(term => {
    tags.add(term);
  });

  return Array.from(tags);
};

/*
 * Note: We get around CORS via https://cors-anywhere.herokuapp.com/
 */
export default async url => {
  console.log("[linkParser] beginning to parse url: ", url);
  let metadata;
  const response = await fetch(`https://xanydl.herokuapp.com/${url}`, {
    mode: "cors",
    redirect: "follow",
    referrer: "no-referrer"
  });

  console.log("fetch returned: ", response);

  if (response.ok) {
    const html = await response.text();
    console.log("[linkParser] response ok for url: ", url);
    metadata = getMetadata(html, url);
    console.log("[linkParser] metadata: ", metadata);
  } else {
    return null;
  }

  const keywords = await parseKeywords(metadata);
  console.log("[linkParser] keywords: ", keywords);

  return { metadata, keywords };
};
