import React from "react";
import Dat from "dat-js";

export const DatContext = React.createContext();
export const DatContextConsumer = DatContext.Consumer;

const initDatRepository = (dat, checksum) => {
  const datRepository = dat.create({ persist: true });
  console.log("Created new persisted datRepository: ", datRepository);

  datRepository.writeFile("/checksum.txt", checksum, () => {
    console.log("Wrote checksum: ", checksum, " to:", datRepository.url);
    localStorage.setItem("dlynx", datRepository.url);
    console.log(`Created dat repo: ${datRepository.url}`);
  });

  return datRepository;
};

// const checkDatRepository = async (datRepository, checksum, cb) => {
//   console.log(
//     "Checking datRepository: ",
//     datRepository.url,
//     datRepository.discoveryKey,
//     datRepository.writable,
//     datRepository.metadata.length
//   );

//   // if (!datRepository.writable && !datRepository.metadata.length) {
//   //   // wait to receive a first update
//   //   await new Promise((resolve, reject) => {
//   //     datRepository.metadata.update(err => {
//   //       console.log("...updating metadata as a check...", err);
//   //       if (err) reject(err);
//   //       else resolve();
//   //     });
//   //   });
//   // }

//   const initialized = await new Promise((resolve, reject) => {
//     datRepository.readFile("/checksum.txt", "utf-8", (err, data) => {
//       if (err) {
//         console.log("err getting checksum: ", err);
//         return reject(false);
//       }

//       if (data !== checksum) {
//         console.log("failed checksum! data was: ", data, checksum);
//         return reject(false);
//       }
//       resolve(true);
//     });
//   });

//   return initialized;
// };

const initAndCheckDatRepository = async () => {
  const dat = new Dat();
  const checksum = Math.random().toString();

  let datRepository;

  const datRepositoryUrl = localStorage.getItem("dlynx");
  if (!datRepositoryUrl) {
    datRepository = initDatRepository(dat, checksum);

    // try {
    //   const initialized = checkDatRepository(datRepository, checksum);
    //   if (!initialized) throw new Error("Unable to initialize App!");
    // } catch (error) {
    //   console.log("Failed checksum :(", error);
    // }
    return datRepository;
  }

  console.log("Found existing datRepository: ", datRepositoryUrl);

  datRepository = dat.get(datRepositoryUrl, { persist: true });

  if (!datRepository) {
    datRepository = initDatRepository(dat, checksum);

    // try {
    //   const initialized = checkDatRepository(datRepository, checksum);
    //   if (!initialized) throw new Error("Unable to initialize App!");
    // } catch (error) {
    //   console.log("Failed checksum :(", error);
    // }
    return datRepository;
  }

  // try {
  //   const initialized = checkDatRepository(datRepository, checksum);
  //   if (!initialized) throw new Error("Unable to initialize App!");
  // } catch (error) {
  //   console.log("Failed checksum :(", error);
  // }

  return datRepository;
};

class DatContextProvider extends React.Component {
  state = {
    datRepository: null
  };

  componentWillMount() {
    const datRepository = initAndCheckDatRepository();

    this.setState({
      datRepository
    });
  }

  render() {
    return (
      <DatContext.Provider
        value={{
          data: { ...this.state },
          actions: {}
        }}
      >
        {this.props.children}
      </DatContext.Provider>
    );
  }
}

export default DatContextProvider;
